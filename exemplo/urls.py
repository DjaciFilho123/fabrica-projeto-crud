from django.urls import path

from exemplo.views import viewCriar

urlpatterns = [
    path('criar/', viewCriar, name='criar'),
]