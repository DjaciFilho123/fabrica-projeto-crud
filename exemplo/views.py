from django.shortcuts import render
from .forms import RegistroCliente

def viewCriar(request):
    formulario = RegistroCliente(request.POST, request.FILES,)
    if formulario.is_valid():
        formulario.save()
        
    return render(request, "criar.html", {"formulario":formulario})
# Create your views here.
