from django.forms import ModelForm
from .models import Cliente

class RegistroCliente(ModelForm):
    class Meta:
        model = Cliente
        fields = ['nome', 
        'data_nascimento', 
        'pontuacao', 'habilidade', 
        'observacao']